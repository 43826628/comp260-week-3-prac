# README #

The first prac exercise for COMP260 students. 

### Task ###

1. Fork a copy of this repository into your private account. This gives you a private copy of the project which you can work with in your own account.
2. Clone the project onto your local computer. This gives you a local copy of the project on your machine to edit.
3. Edit the project. Open the project and edit the script so it prints “Hello World” to the Console when the player clicks on the cube.
4. Push your changes. This copies the modified files back to your private repository.
5. Change computers. 
6. Clone the repository on the new computer.
7. Edit the project again. Edit the script so it prints “Goodbye” when the player quits.
8. Push your changes. 
9. Change back to the original computer.
10. Pull the changes.
11. Submit your code.

See the prac sheet for details